'use-strict'

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('comp-sass', function() {
  return gulp.src('app/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
})
