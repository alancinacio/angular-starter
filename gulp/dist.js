'use strict';

var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var concat = require('gulp-concat');
var browserify = require('browserify');
var debowerify = require('debowerify');
var stringify = require('stringify');
var size = require('gulp-size');
var source = require('vinyl-source-stream');
var streamify = require('streamify');
var requireGlobify = require('require-globify');

gulp.task('dist-images', function(){
  return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
  .pipe(imagemin({
      // Setting interlaced to true
      interlaced: true
    }))
  .pipe(gulp.dest('dist/images'))
});

gulp.task('dist-sass', function() {
  return gulp.src('app/css/**/*.css')
    .pipe(concat('main.css'))
    .pipe(gulp.dest('dist/css'))
})

gulp.task('dist-lib', [], function () {
    var b = browserify(
        {
            entries: './lib-index.js',
            debug: false
        }
    ).transform(stringify(['.html']))
        .transform(debowerify);

    return b.bundle()
        .pipe(source('index.js'))
        .pipe(gulp.dest('dist/lib/'));
});

gulp.task('dist-js', function() {
  var b = browserify(
        {
            entries: './app/js/app.js'
        }
    ).transform(stringify(['.html']))
        .transform(requireGlobify)
        .transform(debowerify);
  
  return b.bundle()
    .pipe(source('index.js'))
    .pipe(gulp.dest('dist/js'))
})